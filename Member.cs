﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace usernamePasswordChecker
{
    /*Ideally should move this somewhere more logically in the solution for better architecture
     * Ideally Enum names would be shorter - I don't normally put any jokes in my code (except the occationl comment)
     * but there is a good reaon to try and stand out in a technical test*/
    public enum PasswordRating
    {
        Empty,
        NextToUseless,
        Weak,
        NotTerribleButCouldDoBetter,
        NowYouAreGettingTherePrettyGoodPassword,
        ChuckNorrisWouldBeProudExcellentPassword
    }

    public enum UsernamePasswordSimilarity
    {
        BlankUsernameOrPassword,
        SomewhatSimilar,
        VerySimilar,
        TheSame,
    }

    public class Member
    {
        private static List<string> commonPasswords = new List<string>() { "123456", "password", "12345678", "qwerty", "123456789", "12345", "1234", "111111", "1234567", "dragon", "123123", "baseball", "abc123", "football", "monkey", "letmein", "696969", "shadow", "master", "666666", "qwertyuiop", "123321", "mustang", "1234567890", "michael", "654321", "pussy", "superman", "1qaz2wsx", "7777777", "fuckyou", "121212", "0", "qazwsx", "123qwe", "killer", "trustno1", "jordan", "jennifer", "zxcvbnm", "asdfgh", "hunter", "buster", "soccer", "harley", "batman", "andrew", "tigger", "sunshine", "iloveyou", "fuckme", "2000", "charlie", "robert", "thomas", "hockey", "ranger", "daniel", "starwars", "klaster", "112233", "george", "asshole", "computer", "michelle", "jessica", "pepper", "1111", "zxcvbn", "555555", "11111111", "131313", "freedom", "777777", "pass", "fuck", "maggie", "159753", "aaaaaa", "ginger", "princess", "joshua", "cheese", "amanda", "summer", "love", "ashley", "6969", "nicole", "chelsea", "biteme", "matthew", "access", "yankees", "987654321", "dallas", "austin", "thunder", "taylor", "matrix" };

        public static int GetUsernamePasswordSimilar(string username, string password)
        {
            return LevenshteinDistance.Compute(username, password);

            /*The below is example code that could be used for username and password comparison but 
             * anything more than basic compairson should be handeled with an already written algorithm. No point reinventing the wheel.
             * I chose the LevenshteinDistance as it appeared to be the most recommended after research*/
            //if(username.Length == 0 || password.Length == 0)
            //{
            //    return UsernamePasswordSimilarity.BlankUsernameOrPassword;
            //}

            ////Exactly the same
            //if(username == password)
            //{
            //    return UsernamePasswordSimilarity.TheSame;
            //}

            //if (username.ToLower() == password.ToLower() || username.Contains(password) || password.Contains(username))
            //{
            //    return UsernamePasswordSimilarity.VerySimilar;
            //}

            //return UsernamePasswordSimilarity.SomewhatSimilar;
        }

        public static PasswordRating GetPasswordStrength(string password)
        {
            int length = password.Length;
            int strength = 0;

            //Argument validation
            if (password == null)
            {
                //todo: We could log this if it is truely exceptional
                throw new ArgumentNullException("password");
            }

            if (length == 0)
            {
                return PasswordRating.Empty;
            }

            if (length < 5)
            {
                return PasswordRating.NextToUseless;
            }

            //Still fairly short or common password has been used. todo: Check for common character replacement e.g '5' for 's'
            if (length < 8 || commonPasswords.Contains(password.ToLower()))
            {
                return PasswordRating.Weak;
            }

            //Increase score for password requirement
            //Contains numbers
            if (password.Any(char.IsDigit))
            {
                strength++;
            }

            //Contains upper and lowercase
            if (password.Any(char.IsUpper) && password.Any(char.IsLower))
            {
                strength++;
            }

            //Contains non-alphanumeric
            if (password.Any(x => !char.IsLetterOrDigit(x)))
            {
                strength++;
            }

            switch (strength)
            {
                case 1:
                    return PasswordRating.NotTerribleButCouldDoBetter;
                case 2:
                    return PasswordRating.NowYouAreGettingTherePrettyGoodPassword;
                case 3:
                    return PasswordRating.ChuckNorrisWouldBeProudExcellentPassword;
                default:
                    return PasswordRating.Weak;
            }
        }
    }
}
