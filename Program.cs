﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace usernamePasswordChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            //infinte loop for debugging
            while (true)
            {
                /*Trim leading and trailing white space as we don't want to bother evaluating only whitespace and 
                 * users normally accidentally enter white space at the begining or end*/
                Console.WriteLine("Username");
                var username = Console.ReadLine().Trim();

                //todo: Asterisk out password
                Console.WriteLine("Password");
                var password = Console.ReadLine().Trim();

                Console.WriteLine();

                Console.WriteLine($"Username and password similarity (LevenshteinDistance): {Member.GetUsernamePasswordSimilar(username, password)}");

                Console.WriteLine($"Password strength: {Member.GetPasswordStrength(password).ToString()}" + Environment.NewLine);
            }
        }
    }
}
